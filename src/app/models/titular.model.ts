import { CustodioModel } from "./custodio.model";

export class TitularModel {
    titularId?: number;
    tipoCustodio?: string;
    tipoCustodioDes?: string;
    nroTituloHabilitante?: string;
    vigenciaInicio?: string;
    vigenciaFin?: string;
    nombreTituloHabilitante?: string;
    tipoPersona?: string;
    tipoPersonaDes?: string;
    nombreTitularComunidad?: string;
    dniRucTitular?: string;
    nombreRepLegal?: string;
    dniRucRepLegal?: string;
    ambitoTerritorial?: string;
    ubigeoId?: string;
    departamento?: string;
    provincia?: string;
    distrito?: string;
    departamentoNombre?: string;
    provinciaNombre?: string;
    distritoNombre?: string;
    nombreUbigeo?: string;
    extension?: string;
    nroMiembroComite?: number;
    comiteActoReconocimiento?: string;
    fechaActoReconocimiento?: string;
    vigencia?: string;
    periodoDesde?: string;
    periodoHasta?: string;
    fechaSolicitud?: string;
    nombreAdjunto?: string;
    rutaAdjunto?: string;
    custodioNombres?: string;
    custodioCarne?: string;
    custodioActoAdministrativo?: string;
    custodioActoAdministrativoFecha?: string;
    estado?: number;
    usuarioRegistro?: number;
    fechaRegistro?: string;
    totalRegistros?: number;
    totalPaginas?: number;
    accion?: string;
    
    custodio?: CustodioModel[];
    comiteRenovacionId?: number;
    
    correoTitular?: string;
    celularTitular?: string;
    fechaActualizacion?: string;

    tipoPlazo?: string;
    tipoPlazoDes?: string;

    /*INICIO: Campos agregados el 18 mayo del 2023 */
    cuenca?: string;
    cesionEnUso?: string;
    numeroContrato?: string;
    nombreAdjuntoContrato?: string;
    rutaAdjuntoContrato?: string;
    ubigeoOtroId?: string;
    departamentoOtro?: string;
    provinciaOtro?: string;
    distritoOtro?: string;
    departamentoNombreOtro?: string;
    provinciaNombreOtro?: string;
    distritoNombreOtro?: string;
    nombreUbigeoOtro?: string;
    /*FIN: Campos agregados el 18 mayo del 2023 */
}