import { TitularModel } from "./titular.model";

export class ComunidadModel {
    accion?: string;
    titularId?: number;
    secuenciaId?: number;
    nroTituloHabilitante?: string;
    tituloPropiedad?: string;
    vigenciaInicio?: string;
    vigenciaFin?: string;
    estado?: number;
    usuarioRegistro?: number;
    existeVigencia?: number;
    flagPermisoForestal?: number;
}