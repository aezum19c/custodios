import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultaPublicaLoretoComponent } from './components/consulta-publica-loreto/consulta-publica-loreto.component';
import { ConsultaPublicaComponent } from './components/consulta-publica/consulta-publica.component';
import { CustodiosComiteComponent } from './components/custodios-comite/custodios-comite.component';
import { CustodiosComponent } from './components/custodios/custodios.component';
import { LoginLoretoComponent } from './components/login-loreto/login-loreto.component';
import { LoginComponent } from './components/login/login.component';
import { MantenimientocustodiosComponent } from './components/mantenimientocustodios/mantenimientocustodios.component';
import { TitularesComponent } from './components/titulares/titulares.component';

const routes: Routes = [
  { path:'login-ucayali', component:LoginComponent},
  { path:'login-loreto', component:LoginLoretoComponent},
  { path:'custodios', component:CustodiosComponent},
  { path:'titulares/:id', component:TitularesComponent},
  { path:'mantenimientocustodios/:id', component:MantenimientocustodiosComponent},
  { path:'custodios-comite', component:CustodiosComiteComponent},
  { path:'consulta-publica-ucayali', component:ConsultaPublicaComponent},
  { path:'consulta-publica-loreto', component:ConsultaPublicaLoretoComponent},
  { path:'**', redirectTo:'login-ucayali', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
