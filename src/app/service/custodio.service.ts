import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_API } from 'src/app/utils/constantes.constant';
import { CustodioModel } from '../models/custodio.model';
import { TitularModel } from '../models/titular.model';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustodiosService {

  constructor(private http: HttpClient) { }

  /*getTitulares( usuarioId : Number, filtro: string, tipoCustodio: string,  pag : Number, regxPag: Number ){
    let url = URL_API + 'titulares?tipo=' + tipoCustodio + '&buscar='+filtro + '&desde='  + '' + '&hasta=' + '' + '&pagina=' + pag + '&regxpag=' + regxPag;
    
    return this.http.get(url);
  }*/

  getTitulares( usuarioId : Number, rolId: string, filtro: string, tipoCustodio: string,  pag : Number, regxPag: Number ){
    let url = URL_API + 'titulares?tipo=' + tipoCustodio + '&buscar='+filtro + '&desde='  + '' + '&hasta=' + '' + '&pagina=' + pag + '&regxpag=' + regxPag;

    const headers = new HttpHeaders({
      'rolId': rolId+'',
    });

    return this.http.get(url, {headers})
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  crearTitular(rolId: string, titular: TitularModel){
    const url = `${ URL_API }crudTitular`;

    const headers = new HttpHeaders({
      'rolId': rolId+'',
    });

    return this.http.post(url, titular, {headers})
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  modificarTitular(rolId: string, titular: TitularModel){
    const url = `${ URL_API }crudTitular`;

    const headers = new HttpHeaders({
      'rolId': rolId+'',
    });

    return this.http.post(url, titular, {headers})
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  activaDesactivarTitular(rolId: string, titular: TitularModel){
    const url = `${ URL_API }crudTitular`;
    let obj = {
      titularId: titular.titularId,
      accion: titular.accion
    }

    const headers = new HttpHeaders({
      'rolId': rolId+'',
    });

    return this.http.post( url, obj, {headers} )
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  /* CUSTODIOS */
  getCustodios( titularId : Number, comiteRenovacionId: Number, filtro:string,  pag : Number, regxPag: Number ){
    let url = URL_API + 'custodios?titularId=' + titularId + '&comiteRenovacionId='+ comiteRenovacionId + '&buscar='+filtro + '&desde='  + '' + '&hasta=' + '' + '&pagina=' + pag + '&regxpag=' + regxPag;
    
    return this.http.get(url);
  }

  crearCustodio(custodio: CustodioModel){
    const url = `${ URL_API }crudCustodio`;
    return this.http.post(url, custodio)
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  modificarCustodio(custodio: CustodioModel){
    const url = `${ URL_API }crudCustodio`;
    return this.http.post(url, custodio)
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  activaDesactivarCustodio(custodio: CustodioModel){
    const url = `${ URL_API }crudCustodio`;
    let obj = {
      titularId: custodio.titularId,
      custodioId: custodio.custodioId,
      accion: custodio.accion
    }
    return this.http.post(url, obj)
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }

  getTitular( titularId : Number ){
    let url = URL_API + 'titularById?titularId=' + titularId;
    
    return this.http.get(url);
  }
}
