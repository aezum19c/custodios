import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { URL_API } from 'src/app/utils/constantes.constant';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  constructor( private http: HttpClient) { }

  getUsuario( usuario: UsuarioModel ){
    const user = {
      ...usuario
    };

    let url = URL_API + 'login';

    return this.http.post(url, user);
  }

  getIniciarLogin( usuario: UsuarioModel, rolId: number){
    const url = `${ URL_API }login`;

    const headers = new HttpHeaders({
      'rolId': rolId+'',
    });

    return this.http.post(url, usuario, {headers})
      .pipe(
        map( resp => {
          return resp;
        })
      );
  }
}